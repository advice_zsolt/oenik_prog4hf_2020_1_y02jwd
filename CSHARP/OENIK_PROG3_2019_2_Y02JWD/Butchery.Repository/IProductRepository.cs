﻿// <copyright file="IProductRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Butchery.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Butchery.Data;

    /// <summary>
    /// Determines the finctionality of the ProductRepository.
    /// </summary>
    public interface IProductRepository : IRepository<PRODUCT>
    {
        /// <summary>
        /// Changes the price of the product with the given ID.
        /// </summary>
        /// <param name="id"> ID of the element which will be changed. </param>
        /// <param name="newPrice"> New price of the product. </param>
        void ChangePrice(int id, int newPrice);
    }
}
