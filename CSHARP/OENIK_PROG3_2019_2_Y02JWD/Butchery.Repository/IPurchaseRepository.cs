﻿// <copyright file="IPurchaseRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Butchery.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Butchery.Data;

    /// <summary>
    /// Interface, determines the functionality of the PurchaseRepository.
    /// </summary>
    public interface IPurchaseRepository : IRepository<PURCHASE>
    {
        /// <summary>
        /// Chnages the purchase time of a given purchase.
        /// </summary>
        /// <param name="id"> ID of the purchase which will be changed. </param>
        /// <param name="newTime"> New purchase time. </param>
        void ChangeTime(int id, DateTime newTime);

        /// <summary>
        /// Deletes an element without saving the table (required because of references between tables).
        /// </summary>
        /// <param name="entity"> PURCHASE entity which is deleted. </param>
        void DeleteWithoutSave(PURCHASE entity);

        /// <summary>
        /// Saves the changes made in the table.
        /// </summary>
        void SaveChange();
    }
}
