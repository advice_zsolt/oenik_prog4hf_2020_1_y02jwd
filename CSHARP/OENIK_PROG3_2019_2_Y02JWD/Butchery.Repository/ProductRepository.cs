﻿// <copyright file="ProductRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Butchery.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Butchery.Data;

    /// <summary>
    /// Sets up the database access for the Product table.
    /// </summary>
    public class ProductRepository : Repository<PRODUCT>, IProductRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProductRepository"/> class.
        /// </summary>
        /// <param name="ctx"> Database Entity. </param>
        public ProductRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <inheritdoc/>
        public void ChangePrice(int id, int newPrice)
        {
            PRODUCT product = this.GetById(id);
            product.PRICE = newPrice;
            this.ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public override void Create(PRODUCT entity)
        {
            this.ctx.Set<PRODUCT>().Add(entity);
            this.ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public override void Delete(PRODUCT entity)
        {
            this.ctx.Set<PRODUCT>().Remove(entity);
            this.ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public override PRODUCT GetById(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.PRODUCT_ID == id);
        }

        /// <inheritdoc/>
        public override void Update(PRODUCT entity)
        {
            PRODUCT product = this.GetById(entity.PRODUCT_ID);
            product = entity;
            this.ctx.SaveChanges();
        }
    }
}
