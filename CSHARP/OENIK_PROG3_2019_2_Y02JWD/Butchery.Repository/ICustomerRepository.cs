﻿// <copyright file="ICustomerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Butchery.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Butchery.Data;

    /// <summary>
    /// Interface, determines the functionality of the CustomerRepository.
    /// </summary>
    public interface ICustomerRepository : IRepository<CUSTOMER>
    {
        /// <summary>
        /// Changes the email address of the given element.
        /// </summary>
        /// <param name="id"> ID of the element which will be changed. </param>
        /// <param name="newEmail"> New email address of the customer. </param>
        void ChangeEmail(int id, string newEmail);
        /// <summary>
        /// Changes the given element.
        /// </summary>
        /// <param name="customerId"> ID</param>
        /// <param name="name"> Name</param>
        /// <param name="age"> Age</param>
        /// <param name="gender"> Gender</param>
        /// <param name="email"> Email</param>
        /// <param name="weight"> Weight</param>
        /// <returns></returns>
        bool ChangeCustomerObj(int customerId, string name, int age, string gender, string email, int weight);
    }
}
