﻿// <copyright file="PurchaseRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Butchery.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Butchery.Data;

    /// <summary>
    /// Sets up the database access for the Purchase table.
    /// </summary>
    public class PurchaseRepository : Repository<PURCHASE>, IPurchaseRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PurchaseRepository"/> class.
        /// </summary>
        /// <param name="ctx"> Database Entity. </param>
        public PurchaseRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <inheritdoc/>
        public void ChangeTime(int id, DateTime newTime)
        {
            PURCHASE purchase = this.GetById(id);
            purchase.PURCHASE_DATE = newTime;
            this.ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public override void Create(PURCHASE entity)
        {
            this.ctx.Set<PURCHASE>().Add(entity);
            this.ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public override void Delete(PURCHASE entity)
        {
            this.ctx.Set<PURCHASE>().Remove(entity);
            this.ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void DeleteWithoutSave(PURCHASE entity)
        {
            this.ctx.Set<PURCHASE>().Remove(entity);
        }

        /// <inheritdoc/>
        public override PURCHASE GetById(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.PURCHASE_ID == id);
        }

        /// <inheritdoc/>
        public void SaveChange()
        {
            this.ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public override void Update(PURCHASE entity)
        {
            PURCHASE purchase = this.GetById(entity.PURCHASE_ID);
            purchase = entity;
            this.ctx.SaveChanges();
        }
    }
}
