﻿// <copyright file="CustomerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Butchery.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Butchery.Data;

    /// <summary>
    /// Sets up the database access for the Customer table.
    /// </summary>
    public class CustomerRepository : Repository<CUSTOMER>, ICustomerRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerRepository"/> class.
        /// </summary>
        /// <param name="ctx"> Database Entity. </param>
        public CustomerRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <inheritdoc/>
        public void ChangeEmail(int id, string newEmail)
        {
            CUSTOMER customer = this.GetById(id);
            customer.EMAIL = newEmail;
            this.ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public override void Create(CUSTOMER entity)
        {
            this.ctx.Set<CUSTOMER>().Add(entity);
            this.ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public override void Delete(CUSTOMER entity)
        {
            this.ctx.Set<CUSTOMER>().Remove(entity);
            this.ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public override CUSTOMER GetById(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.CUSTOMER_ID == id);
        }

        /// <inheritdoc/>
        public override void Update(CUSTOMER entity)
        {
            CUSTOMER customer = this.GetById(entity.CUSTOMER_ID);
            customer = entity;
            this.ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public bool ChangeCustomerObj(int customerId, string name, int age, string gender, string email, int weight)
        {
            CUSTOMER editCustomer = GetById(customerId);
            if (editCustomer == null) return false;
            editCustomer.NAME = name;
            editCustomer.AGE = age;
            editCustomer.GENDER = gender;
            editCustomer.EMAIL = email;
            editCustomer.CUSTOMER_WEIGHT = weight;
            ctx.SaveChanges();
            return true;
        }
    }
}