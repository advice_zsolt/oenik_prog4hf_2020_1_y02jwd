﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Butchery.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Generic repository of the database.
    /// </summary>
    /// <typeparam name="T"> Generic type. </typeparam>
    public abstract class Repository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// DbContext instance.
        /// </summary>
        protected DbContext ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{T}"/> class.
        /// </summary>
        /// <param name="ctx"> DbContext instance. </param>
        public Repository(DbContext ctx)
        {
            this.ctx = ctx;
        }

        /// <summary>
        /// Gets all elements in the table.
        /// </summary>
        /// <returns> Returns all elements of the table. </returns>
        public IQueryable<T> GetAll()
        {
            return this.ctx.Set<T>();
        }

        /// <summary>
        /// Saves the changes made to the table.
        /// </summary>
        public void SaveChanges()
        {
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Gets the element with the given ID.
        /// </summary>
        /// <param name="id"> ID of the requested element. </param>
        /// <returns> T, element with the given ID. </returns>
        public abstract T GetById(int id);

        /// <summary>
        /// Puts a new element in the table.
        /// </summary>
        /// <param name="entity"> T entity, which is put into the table. </param>
        public abstract void Create(T entity);

        /// <summary>
        /// Updates a given element of the table.
        /// </summary>
        /// <param name="entity"> T entity, which is updated. </param>
        public abstract void Update(T entity);

        /// <summary>
        /// Deletes the given element from the table.
        /// </summary>
        /// <param name="entity"> T entity, which is deleted from the table. </param>
        public abstract void Delete(T entity);
    }
}
