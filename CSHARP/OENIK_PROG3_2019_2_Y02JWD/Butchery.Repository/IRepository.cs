﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Butchery.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface, that determines the common functionalities of the repositories.
    /// </summary>
    /// <typeparam name="T"> Generic type. </typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Puts a new element in the table.
        /// </summary>
        /// <param name="entity"> T entity, which is put into the table. </param>
        void Create(T entity);

        /// <summary>
        /// Gets the element with the given ID.
        /// </summary>
        /// <param name="id"> ID of the requested element. </param>
        /// <returns> T, element with the given ID. </returns>
        T GetById(int id);

        /// <summary>
        /// Gets all elements in the table.
        /// </summary>
        /// <returns> Returns all elements of the table. </returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Updates a given element of the table.
        /// </summary>
        /// <param name="entity"> T entity, which is updated. </param>
        void Update(T entity);

        /// <summary>
        /// Deletes the given element from the table.
        /// </summary>
        /// <param name="entity"> T entity, which is deleted from the table. </param>
        void Delete(T entity);

        /// <summary>
        /// Saves the changes made to the table.
        /// </summary>
        void SaveChanges();
    }
}
