﻿// <copyright file="LocationRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Butchery.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Butchery.Data;

    /// <summary>
    /// Sets up the database access for the Location table.
    /// </summary>
    public class LocationRepository : Repository<LOCATION>, ILocationRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LocationRepository"/> class.
        /// </summary>
        /// <param name="ctx"> Database Entity. </param>
        public LocationRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <inheritdoc/>
        public void ChangeWorkerCount(int id, int newCount)
        {
            LOCATION location = this.GetById(id);
            location.WORKER_COUNT = newCount;
            this.ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public override void Create(LOCATION entity)
        {
            this.ctx.Set<LOCATION>().Add(entity);
            this.ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public override void Delete(LOCATION entity)
        {
            this.ctx.Set<LOCATION>().Remove(entity);
            this.ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public override LOCATION GetById(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.LOCATION_ID == id);
        }

        /// <inheritdoc/>
        public override void Update(LOCATION entity)
        {
            LOCATION location = this.GetById(entity.LOCATION_ID);
            location = entity;
            this.ctx.SaveChanges();
        }
    }
}
