﻿// <copyright file="ILocationRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Butchery.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Butchery.Data;

    /// <summary>
    /// Interface, determines the functionality of the LocationRepository.
    /// </summary>
    public interface ILocationRepository : IRepository<LOCATION>
    {
        /// <summary>
        /// Changes the worker count of the given location.
        /// </summary>
        /// <param name="id"> ID of the location which will be changed. </param>
        /// <param name="newCount"> New worker count of the location. </param>
        void ChangeWorkerCount(int id, int newCount);
    }
}
