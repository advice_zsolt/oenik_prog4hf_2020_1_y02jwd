﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Butchery.ConsolerClient
{
    public class Customer
    {
        
        public int Id { get; set; }        
        public string Name { get; set; }        
        public int Age { get; set; }        
        public string Gender { get; set; }        
        public string Email { get; set; }        
        public int Weight { get; set; }

        public override string ToString()
        {
            return $"ID={Id}, \tName: {Name}, \tAge: {Age}" +
                $"\n\tWeight: {Weight}, \tEmail: {Email}";
        }
    }
}
