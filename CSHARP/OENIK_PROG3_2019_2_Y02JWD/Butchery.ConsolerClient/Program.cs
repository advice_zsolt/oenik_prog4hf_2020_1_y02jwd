﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Butchery.ConsolerClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Waiting");
            Console.ReadLine();

            string url = "https://localhost:44367/api/CustomerApi/";

            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Customer>>(json);
                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }
                Console.WriteLine("\npress any key to continue\n");
                Console.ReadKey();

                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Customer.Name), "Jack");
                postData.Add(nameof(Customer.Age), "30");
                postData.Add(nameof(Customer.Gender), "Male");
                postData.Add(nameof(Customer.Email), "jackie@chan.com");
                postData.Add(nameof(Customer.Weight), "75");

                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                Console.WriteLine("Add:" + response);
                json = client.GetStringAsync(url + "all").Result;
                //Console.WriteLine("All:" + json);
                var list1 = JsonConvert.DeserializeObject<List<Customer>>(json);
                foreach (var item in list1)
                {
                    Console.WriteLine(item);
                }
                Console.WriteLine("\npress any key to continue\n");
                Console.ReadKey();


                int customerId = JsonConvert.DeserializeObject<List<Customer>>(json).Single(x => x.Name == "Jack").Id;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Customer.Id), customerId.ToString());
                postData.Add(nameof(Customer.Name), "Jack");
                postData.Add(nameof(Customer.Age), "30");
                postData.Add(nameof(Customer.Gender), "Male");
                postData.Add(nameof(Customer.Email), "jackie@chan.com");
                postData.Add(nameof(Customer.Weight), "90");

                response = client.PostAsync(url + "mod", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                Console.WriteLine("Add:" + response);
                json = client.GetStringAsync(url + "all").Result;
                //Console.WriteLine("All:" + json);
                var list2 = JsonConvert.DeserializeObject<List<Customer>>(json);
                foreach (var item in list2)
                {
                    Console.WriteLine(item);
                }
                Console.WriteLine("\npress any key to continue\n");
                Console.ReadKey();

                Console.WriteLine("DEL: " + client.GetStringAsync(url + "del/" + customerId).Result);
                json = client.GetStringAsync(url + "all").Result;
                var list3 = JsonConvert.DeserializeObject<List<Customer>>(json);
                foreach (var item in list3)
                {
                    Console.WriteLine(item);
                }
                //Console.WriteLine("All: " + client.GetStringAsync(url + "all").Result);
                Console.WriteLine("\npress any key to continue\n");
                Console.ReadKey();
            }

            Console.WriteLine("the end");
            Console.ReadLine();
        }
    }
}
