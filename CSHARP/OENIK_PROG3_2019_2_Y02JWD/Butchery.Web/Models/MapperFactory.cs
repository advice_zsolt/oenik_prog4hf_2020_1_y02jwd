﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Butchery.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Butchery.Data.CUSTOMER, Butchery.Web.Models.Customer>().
                    ForMember(dest => dest.Id, map => map.MapFrom(src => src.CUSTOMER_ID)).
                    ForMember(dest => dest.Name, map => map.MapFrom(src => src.NAME)).
                    ForMember(dest => dest.Age, map => map.MapFrom(src => src.AGE)).
                    ForMember(dest => dest.Gender, map => map.MapFrom(src => src.GENDER)).
                    ForMember(dest => dest.Email, map => map.MapFrom(src => src.EMAIL)).
                    ForMember(dest => dest.Weight, map => map.MapFrom(src => src.CUSTOMER_WEIGHT));
            });
            return config.CreateMapper();
        }
    }
}