﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Permissions;
using System.Web;

namespace Butchery.Web.Models
{
    public class Customer
    {
        [Display(Name = "Customer id")]
        [Required]
        public int Id { get; set; }

        [Display(Name = "Customer name")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Customer age")]
        [Required]
        public int Age { get; set; }

        [Display(Name = "Customer gender")]
        [Required]
        public string Gender { get; set; }

        [Display(Name = "Customer email")]
        [Required]
        public string Email { get; set; }

        [Display(Name = "Customer weight")]
        [Required]
        public int Weight { get; set; }
    }
}