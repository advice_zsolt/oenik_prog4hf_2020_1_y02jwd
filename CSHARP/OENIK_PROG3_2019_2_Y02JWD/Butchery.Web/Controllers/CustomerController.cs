﻿using AutoMapper;
using Butchery.Data;
using Butchery.Logic;
using Butchery.Repository;
using Butchery.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Butchery.Web.Controllers
{
    public class CustomerController : Controller
    {
        IButcheryLogic logic;
        IMapper mapper;
        CustomersViewModel vm;

        public CustomerController()
        {
            ButcheryDBContext ctx = new ButcheryDBContext();
            CustomerRepository customerRepo = new CustomerRepository(ctx);
            logic = new ButcheryLogic();
            mapper = MapperFactory.CreateMapper();
            vm = new CustomersViewModel();
            vm.EditedCustomer = new Customer();
            var customers = logic.GetAllCustomersObj();
            vm.ListOfCustomers = mapper.Map<IList<Data.CUSTOMER>, List<Models.Customer>>(customers);
        }

        private Customer GetCustomerModel(int id)
        {
            CUSTOMER oneCustomer = logic.GetCustomerObj(id);
            return mapper.Map<Data.CUSTOMER, Models.Customer>(oneCustomer);
        }

        // GET: Customer
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("CustomerIndex", vm);
        }

        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {
            return View("CustomerDetails", GetCustomerModel(id));
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        // GET: Customers/Remove/1
        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete failed";
            if (logic.DeleteCustomerObj(id)) TempData["editResult"] = "Delete succesfull";
            return RedirectToAction("Index");
        }

        // GET: Customers/Edit/1
        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedCustomer = GetCustomerModel(id);
            return View("CustomerIndex", vm);
        }

        [HttpPost]
        public ActionResult Edit(Customer customer, string editAction)
        {
            if (ModelState.IsValid && customer != null)
            {
                TempData["editedResult"] = "Edit succesfull";
                if (editAction == "AddNew")
                {
                    logic.CreateCustomer(customer.Name, customer.Age, customer.Gender, customer.Email, customer.Weight);
                }
                else
                {
                    if (!logic.ChangeCustomerObj(customer.Id,customer.Name, customer.Age, customer.Gender, customer.Email, customer.Weight))
                    {
                        TempData["editResult"] = "Edit fail";
                    }
                }
                return RedirectToAction("Index");
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedCustomer = customer;
                return View("CustomerIndex", vm);
            }
        }

    }
}
