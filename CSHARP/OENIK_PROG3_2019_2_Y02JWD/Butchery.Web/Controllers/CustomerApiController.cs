﻿using AutoMapper;
using Butchery.Data;
using Butchery.Logic;
using Butchery.Repository;
using Butchery.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Instrumentation;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Butchery.Web.Controllers
{
    public class CustomerApiController : ApiController
    {
        public class ApiResult 
        { 
            public bool OperationResult { get; set; }
        }

        IButcheryLogic logic;
        IMapper mapper;
        public CustomerApiController()
        {
            ButcheryDBContext ctx = new ButcheryDBContext();
            CustomerRepository customerRepo = new CustomerRepository(ctx);
            logic = new ButcheryLogic();
            mapper = MapperFactory.CreateMapper();
        }

        // GET api/CustomerApi/all
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Customer> GetAll()
        {
            var customers = logic.GetAllCustomersObj();
            return mapper.Map<IList<Data.CUSTOMER>, List<Models.Customer>>(customers);
        }

        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneCustomer(int id)
        {
            var result = logic.DeleteCustomerObj(id);
            return new ApiResult() { OperationResult = result };
        }

        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOneCustomer(Customer customer)
        {
            logic.CreateCustomer(customer.Name, customer.Age, customer.Gender, customer.Email, customer.Weight);
            return new ApiResult() { OperationResult = true };
        }

        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModOneCustomer(Customer customer)
        {
            bool result = logic.ChangeCustomerObj(customer.Id, customer.Name, customer.Age, customer.Gender, customer.Email, customer.Weight);
            return new ApiResult() { OperationResult = result };
        }
    }
}
