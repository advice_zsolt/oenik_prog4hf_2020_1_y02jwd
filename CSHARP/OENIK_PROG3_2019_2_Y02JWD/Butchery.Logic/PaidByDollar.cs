﻿// <copyright file="PaidByDollar.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Butchery.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class for the ButcheryLogic.GetPaidByDollars query.
    /// </summary>
    public class PaidByDollar
    {
        /// <summary>
        /// Gets or sets the name propery.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the location property.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Overrides the ToString method to return data in custom format.
        /// </summary>
        /// <returns> String, data of the class. </returns>
        public override string ToString()
        {
            return $"Location: {this.Location}, Name: {this.Name}";
        }

        /// <summary>
        /// Overrides the Equals method, required for tesing.
        /// </summary>
        /// <param name="obj"> Object, which will be tested for equality. </param>
        /// <returns> Bool, whether the objects are equal or not. </returns>
        public override bool Equals(object obj)
        {
            if (obj is PaidByDollar)
            {
                PaidByDollar other = obj as PaidByDollar;
                return this.Name == other.Name && this.Location == other.Location;
            }

            return false;
        }

        /// <summary>
        /// Overrides the GetHashCode method, required for testing.
        /// </summary>
        /// <returns> Int, hash code. </returns>
        public override int GetHashCode()
        {
            return 0;
        }
    }
}
