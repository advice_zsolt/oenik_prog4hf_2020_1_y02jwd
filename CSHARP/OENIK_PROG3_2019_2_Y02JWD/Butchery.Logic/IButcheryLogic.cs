﻿// <copyright file="IButcheryLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Butchery.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Butchery.Data;

    /// <summary>
    /// Interface, lists the Business Logic methods.
    /// </summary>
    public interface IButcheryLogic
    {
        // customer methods

        /// <summary>
        /// Gets a customer with the given ID.
        /// </summary>
        /// <param name="id"> ID of the requested customer. </param>
        /// <returns> String, data of the customer. </returns>
        string GetCustomer(int id);

        /// <summary>
        /// Gets a customer with the given ID.
        /// </summary>
        /// <param name="id"> ID of the requested customer. </param>
        /// <returns> String, data of the customer. </returns>
        CUSTOMER GetCustomerObj(int id);

        /// <summary>
        /// Gets all customers from the Customer table.
        /// </summary>
        /// <returns> String, data of all customers. </returns>
        string GetAllCustomers();

        /// <summary>
        /// Gets all customers from the Customer table.
        /// </summary>
        /// <returns> String, data of all customers. </returns>
        IList<CUSTOMER> GetAllCustomersObj();

        /// <summary>
        /// Changes the email address of the customer with the given ID.
        /// </summary>
        /// <param name="id"> ID of customer whose email address will be changed. </param>
        /// <param name="newEmail"> New email address of the customer. </param>
        /// <returns> String, result of the change. </returns>
        string ChangeEmail(int id, string newEmail);

        /// <summary>
        /// Deletes the customer with the given ID.
        /// </summary>
        /// <param name="id"> ID of the customer who will be deleted. </param>
        /// <returns> String, result of the delete. </returns>
        string DeleteCustomer(int id);

        /// <summary>
        /// Deletes the customer with the given ID.
        /// </summary>
        /// <param name="id"> ID of the customer who will be deleted. </param>
        /// <returns> Bool, result of the delete. </returns>
        bool DeleteCustomerObj(int id);

        /// <summary>
        /// Creates a new Customer instance, which is put into the database.
        /// </summary>
        /// <param name="name"> Name of the new customer. </param>
        /// <param name="age"> Age of the new customer. </param>
        /// <param name="gender"> Gender of the new customer. </param>
        /// <param name="email"> Email address of the new customer. </param>
        /// <param name="weight"> Weight of the new customer. </param>
        void CreateCustomer(string name, int age, string gender, string email, int weight);

        /// <summary>
        /// Changes a Customer instance.
        /// </summary>
        /// <param name="customerId"> ID of the customer. </param>
        /// <param name="name"> Name of the customer. </param>
        /// <param name="age"> Age of the customer. </param>
        /// <param name="gender"> Gender of the customer. </param>
        /// <param name="email"> Email address of the customer. </param>
        /// <param name="weight"> Weight of the customer. </param>
        /// <returns> Bool, result of the change. </returns>
        bool ChangeCustomerObj(int customerId, string name, int age, string gender, string email, int weight);

        // -----------------------------------------------------------------------------------------------------
        // location methods

        /// <summary>
        /// Gets the location with the given ID.
        /// </summary>
        /// <param name="id"> ID of the requested location. </param>
        /// <returns> String, data of the location. </returns>
        string GetLocation(int id);

        /// <summary>
        /// Gets all locations from the Location table.
        /// </summary>
        /// <returns> String, data of all locations. </returns>
        string GetAllLocations();

        /// <summary>
        /// Changes the worker count of the location with the given ID.
        /// </summary>
        /// <param name="id"> ID of the location which will be changed. </param>
        /// <param name="newCount"> New worker count of the location. </param>
        /// <returns> String, result of the change. </returns>
        string ChangeWorkerCount(int id, int newCount);

        /// <summary>
        /// Deletes the location with the given ID.
        /// </summary>
        /// <param name="id"> ID of the location which will be deleted. </param>
        /// <returns> String, result of the delete. </returns>
        string DeleteLocation(int id);

        /// <summary>
        /// Creates a new Location instance.
        /// </summary>
        /// <param name="cashier"> Cashier of the new location. </param>
        /// <param name="name"> Name of the new location. </param>
        /// <param name="currency"> Currency of the new location. </param>
        /// <param name="area"> Area of the new location. </param>
        /// <param name="workerCount"> Worker count of the new location. </param>
        void CreateLocation(string cashier, string name, string currency, int area, int workerCount);

        // -----------------------------------------------------------------------------------------------------
        // product methods

        /// <summary>
        /// Gets the product with the given ID.
        /// </summary>
        /// <param name="id"> ID of the requested produvt. </param>
        /// <returns> String, data of the product. </returns>
        string GetProduct(int id);

        /// <summary>
        /// Gets all products from the Product table.
        /// </summary>
        /// <returns> String, data of all products. </returns>
        string GetAllProducts();

        /// <summary>
        /// Changes the price of the product with the given ID.
        /// </summary>
        /// <param name="id"> ID of the product which will be changed. </param>
        /// <param name="newPrice"> New price of the product. </param>
        /// <returns> String, result of the chanhge. </returns>
        string ChangePrice(int id, int newPrice);

        /// <summary>
        /// Deletes the product with the given ID.
        /// </summary>
        /// <param name="id"> ID of the product which will be deleted. </param>
        /// <returns> String, result of the delete. </returns>
        string DeleteProduct(int id);

        /// <summary>
        /// Cretares a new Product instance.
        /// </summary>
        /// <param name="name"> Name of the new product. </param>
        /// <param name="price"> Price of the new product. </param>
        /// <param name="quality"> Quality of the new product. </param>
        /// <param name="mass"> MAss of the new product. </param>
        /// <param name="origin"> Origin of the new product. </param>
        void CreateProduct(string name, int price, string quality, int mass, string origin);

        // -----------------------------------------------------------------------------------------------------
        // purchase methods

        /// <summary>
        /// Gets the purchase with the given ID.
        /// </summary>
        /// <param name="id"> ID of the requested purchase. </param>
        /// <returns> String, data of the purchase. </returns>
        string GetPurchase(int id);

        /// <summary>
        /// Gets all purcahses from the Purchase table.
        /// </summary>
        /// <returns> String, data of all purchases. </returns>
        string GetAllPurchases();

        /// <summary>
        /// Changes the purchase time of the pruchase with the given ID.
        /// </summary>
        /// <param name="id"> ID of the purchase which will be changed. </param>
        /// <param name="newTime"> New time of the purcahse. </param>
        /// <returns> String, result of the change. </returns>
        string ChangeTime(int id, DateTime newTime);

        /// <summary>
        /// Deletes the purcahse with the given ID.
        /// </summary>
        /// <param name="id"> ID of the purchase which will deleted. </param>
        /// <returns> String, result of the delete. </returns>
        string DeletePurchase(int id);

        /// <summary>
        /// Creates a new Purchase instance.
        /// </summary>
        /// <param name="customer_id"> Customer id of the new purchase. </param>
        /// <param name="product_id"> Product id of the new purchase. </param>
        /// <param name="location_id"> Location id of the new purchase. </param>
        /// <param name="time">Time of the new purchase. </param>
        /// <returns> String, result of the create. </returns>
        string CreatePurchase(int customer_id, int product_id, int location_id, DateTime time);

        // -----------------------------------------------------------------------------------------------------
        // non-crud methods

        /// <summary>
        /// Average age of the customers for each gender.
        /// </summary>
        /// <returns> List, genders and average age. </returns>
        IList<CustomerAgeByGender> GetCustomerAgeByGender();

        /// <summary>
        /// Number of locations for each currency.
        /// </summary>
        /// <returns> List, currency and location count. </returns>
        IList<LocationsPerCurrency> GetLocationsPerCurrency();

        /// <summary>
        /// Purchases in which dollar was used for paying.
        /// </summary>
        /// <returns> LIst, location and customer name. </returns>
        IList<PaidByDollar> GetPaidByDollars();

        /// <summary>
        /// Largest location based on worker count.
        /// </summary>
        /// <returns> List, location, worker count. </returns>
        IList<LargestLocation> GetLargestLocations();

        // -----------------------------------------------------------------------------------------------------
        // java endpoint

        /// <summary>
        /// Generates random customers using the java endpoint.
        /// </summary>
        /// <param name="count"> Count of generated customers. </param>
        /// <returns> String, data of the customers. </returns>
        string JavaGenerator(int count);
    }
}
