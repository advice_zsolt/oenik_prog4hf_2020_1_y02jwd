﻿// <copyright file="LocationsPerCurrency.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Butchery.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class for the ButcheryLogic.GetLocationsPerCurrency query.
    /// </summary>
    public class LocationsPerCurrency
    {
        /// <summary>
        /// Gets or sets the locationCount property.
        /// </summary>
        public int LocationCount { get; set; }

        /// <summary>
        /// Gets or sets the currency property.
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Overrides the ToString method to return data in custom format.
        /// </summary>
        /// <returns> String, data of the class. </returns>
        public override string ToString()
        {
            return $"Curreny: {this.Currency}, Number of locations: {this.LocationCount}";
        }

        /// <summary>
        /// Overrides the Equals method, required for tesing.
        /// </summary>
        /// <param name="obj"> Object, which will be tested for equality. </param>
        /// <returns> Bool, whether the objects are equal or not. </returns>
        public override bool Equals(object obj)
        {
            if (obj is LocationsPerCurrency)
            {
                LocationsPerCurrency other = obj as LocationsPerCurrency;
                return this.LocationCount == other.LocationCount && this.Currency == other.Currency;
            }

            return false;
        }

        /// <summary>
        /// Overrides the GetHashCode method, required for testing.
        /// </summary>
        /// <returns> Int, hash code. </returns>
        public override int GetHashCode()
        {
            return 0;
        }
    }
}
