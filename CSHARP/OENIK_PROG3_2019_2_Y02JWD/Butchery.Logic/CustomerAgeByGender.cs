﻿// <copyright file="CustomerAgeByGender.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Butchery.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class for the ButcheryLogic.GetCustomerAgeByGender query.
    /// </summary>
    public class CustomerAgeByGender
    {
        /// <summary>
        /// Gets or sets the gender property.
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the avgAge property.
        /// </summary>
        public double AvgAge { get; set; }

        /// <summary>
        /// Overrides the ToString method to return data in custom format.
        /// </summary>
        /// <returns> String, data of the class. </returns>
        public override string ToString()
        {
            return $"Gender: {this.Gender}, Average age: {this.AvgAge}";
        }

        /// <summary>
        /// Overrides the Equals method, required for tesing.
        /// </summary>
        /// <param name="obj"> Object, which will be tested for equality. </param>
        /// <returns> Bool, whether the objects are equal or not. </returns>
        public override bool Equals(object obj)
        {
            if (obj is CustomerAgeByGender)
            {
                CustomerAgeByGender other = obj as CustomerAgeByGender;
                return this.Gender == other.Gender && this.AvgAge == other.AvgAge;
            }

            return false;
        }

        /// <summary>
        /// Overrides the GetHashCode method, required for testing.
        /// </summary>
        /// <returns> Int, hash code. </returns>
        public override int GetHashCode()
        {
            return 0;
        }
    }
}
