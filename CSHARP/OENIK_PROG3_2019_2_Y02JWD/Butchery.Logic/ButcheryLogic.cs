﻿// <copyright file="ButcheryLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Butchery.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using Butchery.Data;
    using Butchery.Repository;

    /// <summary>
    /// Business Logic.
    /// </summary>
    public class ButcheryLogic : IButcheryLogic
    {
        private ICustomerRepository customerRepo;
        private ILocationRepository locationRepo;
        private IProductRepository productRepo;
        private IPurchaseRepository purchaseRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ButcheryLogic"/> class.
        /// </summary>
        public ButcheryLogic()
        {
            this.customerRepo = new CustomerRepository(new ButcheryDBContext());
            this.locationRepo = new LocationRepository(new ButcheryDBContext());
            this.productRepo = new ProductRepository(new ButcheryDBContext());
            this.purchaseRepo = new PurchaseRepository(new ButcheryDBContext());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ButcheryLogic"/> class.
        /// Used for testing.
        /// </summary>
        /// <param name="repo"> Mocked repository. </param>
        public ButcheryLogic(ICustomerRepository repo)
        {
            this.customerRepo = repo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ButcheryLogic"/> class.
        /// Used for testing.
        /// </summary>
        /// <param name="repo"> Mocked repository. </param>
        public ButcheryLogic(IPurchaseRepository repo)
        {
            this.purchaseRepo = repo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ButcheryLogic"/> class.
        /// Used for testing.
        /// </summary>
        /// <param name="repo"> Mocked Repository. </param>
        public ButcheryLogic(ILocationRepository repo)
        {
            this.locationRepo = repo;
        }

        // -----------------------------------------------------------------------------------------------------
        // customer methods

        /// <inheritdoc/>
        public string ChangeEmail(int id, string newEmail)
        {
            CUSTOMER customer = this.customerRepo.GetById(id);
            if (customer != null)
            {
                this.customerRepo.ChangeEmail(id, newEmail);
                return "the email address was changed succesfully";
            }

            return "no customer found with the given id";
        }

        /// <inheritdoc/>
        public void CreateCustomer(string name, int age, string gender, string email, int weight)
        {
            var customerCollection = this.customerRepo.GetAll().OrderBy(x => x.CUSTOMER_ID);
            if (customerCollection.Count() != 0)
            {
                int lastCustomerId = customerCollection.Max(x => x.CUSTOMER_ID);
                CUSTOMER newCustomer = new CUSTOMER();
                newCustomer.CUSTOMER_ID = lastCustomerId + 1;
                newCustomer.NAME = name;
                newCustomer.AGE = age;
                newCustomer.GENDER = gender;
                newCustomer.EMAIL = email;
                newCustomer.CUSTOMER_WEIGHT = weight;
                this.customerRepo.Create(newCustomer);
            }

            // if there're no customers, the first ones' ID is 1 FOR UNIT TEST
            else
            {
                CUSTOMER newCustomer = new CUSTOMER();
                newCustomer.CUSTOMER_ID = 1;
                newCustomer.NAME = name;
                newCustomer.AGE = age;
                newCustomer.GENDER = gender;
                newCustomer.EMAIL = email;
                newCustomer.CUSTOMER_WEIGHT = weight;
                this.customerRepo.Create(newCustomer);
            }
        }

        /// <inheritdoc/>
        public bool ChangeCustomerObj(int customerId, string name, int age, string gender, string email, int weight)
        {
            return this.customerRepo.ChangeCustomerObj(customerId, name, age, gender, email, weight);
        }

        /// <inheritdoc/>
        public string DeleteCustomer(int id)
        {
            CUSTOMER customer = this.customerRepo.GetById(id);
            if (customer != null)
            {
                var conn_delete = this.purchaseRepo.GetAll().Where(x => x.CUSTOMER_ID == id);
                foreach (var item in conn_delete)
                {
                    this.purchaseRepo.DeleteWithoutSave(item);
                }

                this.purchaseRepo.SaveChange();
                this.customerRepo.Delete(this.customerRepo.GetById(id));
                return "customer was deleted succesfully";
            }

            return "no customer found with the given id";
        }

        /// <inheritdoc/>
        public bool DeleteCustomerObj(int id)
        {
            CUSTOMER customer = this.customerRepo.GetById(id);
            if (customer != null)
            {
                var conn_delete = this.purchaseRepo.GetAll().Where(x => x.CUSTOMER_ID == id);
                foreach (var item in conn_delete)
                {
                    this.purchaseRepo.DeleteWithoutSave(item);
                }

                this.purchaseRepo.SaveChange();
                this.customerRepo.Delete(this.customerRepo.GetById(id));
                return true;
            }

            return false;
        }

        /// <summary>
        /// Additional delete method for testing.
        /// </summary>
        /// <param name="entity"> Customer entity which will be deleted. </param>
        public void DeleteCustomer(CUSTOMER entity)
        {
            if (this.customerRepo.GetById(entity.CUSTOMER_ID) != null)
            {
                this.customerRepo.Delete(entity);
            }
        }

        /// <inheritdoc/>
        public string GetAllCustomers()
        {
            string toReturn = string.Empty;
            var customers = this.customerRepo.GetAll();
            foreach (var item in customers)
            {
                toReturn += $"Id: {item.CUSTOMER_ID}, Name: {item.NAME}, Age: {item.AGE}, " +
                            $"Gender: {item.GENDER}, Email: {item.EMAIL}, Weight: {item.CUSTOMER_WEIGHT}\n";
            }

            return toReturn;
        }

        /// <inheritdoc/>
        public IList<CUSTOMER> GetAllCustomersObj()
        {
            return this.customerRepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public string GetCustomer(int id)
        {
            CUSTOMER customer = this.customerRepo.GetById(id);
            if (customer != null)
            {
                return $"Id: {customer.CUSTOMER_ID}, Name: {customer.NAME}, Age: {customer.AGE}, " +
                    $"Gender: {customer.GENDER}, Email: {customer.EMAIL}, Weight: {customer.CUSTOMER_WEIGHT}";
            }

            return "no customer found with the given id";
        }

        /// <inheritdoc/>
        public CUSTOMER GetCustomerObj(int id)
        {
            return this.customerRepo.GetById(id);
        }

        // -----------------------------------------------------------------------------------------------------
        // location methods

        /// <inheritdoc/>
        public string ChangeWorkerCount(int id, int newCount)
        {
            LOCATION location = this.locationRepo.GetById(id);
            if (location != null)
            {
                this.locationRepo.ChangeWorkerCount(id, newCount);
                return "worker count was succesfully updated";
            }

            return "no location with the given id";
        }

        /// <inheritdoc/>
        public void CreateLocation(string cashier, string name, string currency, int area, int workerCount)
        {
            var locationCollection = this.locationRepo.GetAll().OrderBy(x => x.LOCATION_ID);
            int lastLocationId = locationCollection.Max(x => x.LOCATION_ID);
            LOCATION newLoc = new LOCATION();
            newLoc.LOCATION_ID = lastLocationId + 1;
            newLoc.CASHIER = cashier;
            newLoc.NAME = name;
            newLoc.CURRENCY = currency;
            newLoc.AREA = area;
            newLoc.WORKER_COUNT = workerCount;
            this.locationRepo.Create(newLoc);
        }

        /// <inheritdoc/>
        public string DeleteLocation(int id)
        {
            LOCATION location = this.locationRepo.GetById(id);
            if (location != null)
            {
                var conn_delete = this.purchaseRepo.GetAll().Where(x => x.LOCATION_ID == id);
                foreach (var item in conn_delete)
                {
                    this.purchaseRepo.DeleteWithoutSave(item);
                }

                this.purchaseRepo.SaveChange();
                this.locationRepo.Delete(this.locationRepo.GetById(id));
                return "location was deleted succesfully";
            }

            return "no location with the given id";
        }

        /// <inheritdoc/>
        public string GetAllLocations()
        {
            string toReturn = string.Empty;
            var location = this.locationRepo.GetAll();
            foreach (var item in location)
            {
                toReturn += $"Id: {item.LOCATION_ID}, Cashier: {item.CASHIER}, Name: {item.NAME}, " +
                            $"Currency: {item.CURRENCY}, Area: {item.AREA}, Worker count: {item.WORKER_COUNT}\n";
            }

            return toReturn;
        }

        /// <inheritdoc/>
        public string GetLocation(int id)
        {
            LOCATION location = this.locationRepo.GetById(id);
            if (location != null)
            {
                return $"Id: {location.LOCATION_ID}, Cashier: {location.CASHIER}, Name: {location.NAME}, " +
                    $"Currency: {location.CURRENCY}, Area: {location.AREA}, Worker count: {location.WORKER_COUNT}";
            }

            return "no location found with the given id";
        }

        // -----------------------------------------------------------------------------------------------------
        // product methods

        /// <inheritdoc/>
        public string ChangePrice(int id, int newPrice)
        {
            PRODUCT product = this.productRepo.GetById(id);
            if (product != null)
            {
                this.productRepo.ChangePrice(id, newPrice);
                return "price was succesfully changed";
            }

            return "no product with the given id";
        }

        /// <inheritdoc/>
        public void CreateProduct(string name, int price, string quality, int mass, string origin)
        {
            var productCollection = this.productRepo.GetAll().OrderBy(x => x.PRODUCT_ID);
            int lastProductId = productCollection.Max(x => x.PRODUCT_ID);
            PRODUCT newproduct = new PRODUCT();
            newproduct.PRODUCT_ID = lastProductId + 1;
            newproduct.NAME = name;
            newproduct.PRICE = price;
            newproduct.QUALITY = quality;
            newproduct.MASS = mass;
            newproduct.ORIGIN = origin;
            this.productRepo.Create(newproduct);
        }

        /// <inheritdoc/>
        public string DeleteProduct(int id)
        {
            PRODUCT product = this.productRepo.GetById(id);
            if (product != null)
            {
                var conn_delete = this.purchaseRepo.GetAll().Where(x => x.PRODUCT_ID == id);
                foreach (var item in conn_delete)
                {
                    this.purchaseRepo.DeleteWithoutSave(item);
                }

                this.purchaseRepo.SaveChange();
                this.productRepo.Delete(this.productRepo.GetById(id));
                return "product was deleted sucessfully";
            }

            return "no product with the given id";
        }

        /// <inheritdoc/>
        public string GetAllProducts()
        {
            string toReturn = string.Empty;
            var product = this.productRepo.GetAll();
            foreach (var item in product)
            {
                toReturn += $"Id: {item.PRODUCT_ID},  Name: {item.NAME}, Price: {item.PRICE}, Quality: {item.QUALITY}, " +
                            $"Mass: {item.MASS}, Origin: {item.ORIGIN}\n";
            }

            return toReturn;
        }

        /// <inheritdoc/>
        public string GetProduct(int id)
        {
            PRODUCT product = this.productRepo.GetById(id);
            if (product != null)
            {
                return $"Id: {product.PRODUCT_ID}, Name: {product.NAME}, Price: {product.PRICE}, Quality: {product.QUALITY}, " +
                    $"Mass: {product.MASS}, Origin: {product.ORIGIN}";
            }

            return "no product found with the given id";
        }

        // -----------------------------------------------------------------------------------------------------
        // purchase methods

        /// <inheritdoc/>
        public string ChangeTime(int id, DateTime newTime)
        {
            PURCHASE purchase = this.purchaseRepo.GetById(id);
            if (purchase != null)
            {
                this.purchaseRepo.ChangeTime(id, newTime);
                return "time was changed succesfully";
            }

            return "no purchase with the given id";
        }

        /// <inheritdoc/>
        public string CreatePurchase(int customer_id, int product_id, int location_id, DateTime time)
        {
            var purchaseCollection = this.purchaseRepo.GetAll().OrderBy(x => x.PURCHASE_ID);
            int lastPurchaseId = purchaseCollection.Max(x => x.PURCHASE_ID);

            // checking if the id exits
            if (this.customerRepo.GetById(customer_id) != null &&
                this.productRepo.GetById(product_id) != null &&
                this.locationRepo.GetById(location_id) != null)
            {
                PURCHASE newPurchase = new PURCHASE();
                newPurchase.PURCHASE_ID = lastPurchaseId + 1;
                newPurchase.CUSTOMER_ID = customer_id;
                newPurchase.PRODUCT_ID = product_id;
                newPurchase.LOCATION_ID = location_id;
                newPurchase.PURCHASE_DATE = time;
                this.purchaseRepo.Create(newPurchase);
                return "purchase created succesfully";
            }

            return "customer/product/location id not found";
        }

        /// <inheritdoc/>
        public string DeletePurchase(int id)
        {
            PURCHASE purchase = this.purchaseRepo.GetById(id);
            if (purchase != null)
            {
                this.purchaseRepo.Delete(this.purchaseRepo.GetById(id));
                return "purchase was deleted succesfully";
            }

            return "no purchase with the given id";
        }

        /// <inheritdoc/>
        public string GetAllPurchases()
        {
            string toReturn = string.Empty;
            var purchase = this.purchaseRepo.GetAll();
            foreach (var item in purchase)
            {
                toReturn += $"id: {item.PURCHASE_ID}, Customer: {item.CUSTOMER.NAME}, Product: {item.PRODUCT.NAME}, " +
                            $"Location: {item.LOCATION.NAME}, Date: {item.PURCHASE_DATE}\n";
            }

            return toReturn;
        }

        /// <inheritdoc/>
        public string GetPurchase(int id)
        {
            PURCHASE purchase = this.purchaseRepo.GetById(id);
            if (purchase != null)
            {
                return $"Id: {purchase.PURCHASE_ID}, Customer: {purchase.CUSTOMER.NAME}, Product: {purchase.PRODUCT.NAME}, " +
                    $"Location: {purchase.LOCATION.NAME}, Date: {purchase.PURCHASE_DATE}";
            }

            return "no purchase found with the given id";
        }

        // -----------------------------------------------------------------------------------------------------
        // non-CRUD methods

        /// <inheritdoc/>
        public IList<CustomerAgeByGender> GetCustomerAgeByGender()
        {
            var q = from customers in this.customerRepo.GetAll()
                    group customers by customers.GENDER into grp
                    select new CustomerAgeByGender()
                    {
                        Gender = grp.Key,
                        AvgAge = grp.Average(x => x.AGE) ?? 0,
                    };
            return q.OrderByDescending(x => x.AvgAge).ToList();
        }

        /// <inheritdoc/>
        public IList<LocationsPerCurrency> GetLocationsPerCurrency()
        {
            var q = from locations in this.locationRepo.GetAll()
                    group locations by locations.CURRENCY into grp
                    select new LocationsPerCurrency()
                    {
                        Currency = grp.Key,
                        LocationCount = grp.Count(),
                    };
            return q.ToList();
        }

        /// <inheritdoc/>
        public IList<PaidByDollar> GetPaidByDollars()
        {
            var q = from purchases in this.purchaseRepo.GetAll()
                    where purchases.LOCATION.CURRENCY == "DOLLAR"
                    select new PaidByDollar()
                    {
                        Location = purchases.LOCATION.NAME,
                        Name = purchases.CUSTOMER.NAME,
                    };
            return q.OrderBy(x => x.Name.Length).ToList();
        }

        /// <inheritdoc/>
        public IList<LargestLocation> GetLargestLocations()
        {
            var maxLoc = this.locationRepo.GetAll().Select(x => x.WORKER_COUNT).Max();
            var q = from location in this.locationRepo.GetAll()
                    where location.WORKER_COUNT == maxLoc
                    select new LargestLocation()
                    {
                        Location = location.NAME,
                        WorkerCount = location.WORKER_COUNT ?? 0,
                    };
            return q.ToList();
        }

        /// <inheritdoc/>
        public string JavaGenerator(int input)
        {
            string toRetunr = string.Empty;
            string context = string.Empty;
            WebClient wc = new WebClient();
            try
            {
                context = wc.DownloadString(
                    "http://localhost:8080/Butchery.JavaWeb/CustomerServlet?input=" + input);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            context = context.Replace('"', ' ');
            context = context.Replace('{', ' ');
            context = context.Replace('}', ' ');
            context = context.Replace("  ", string.Empty);
            context = context.Trim('[', ']');
            string[] items = context.Split(',');
            for (int i = 0; i < items.Length; i++)
            {
                toRetunr += items[i] + "\n";
            }

            return toRetunr;
        }
    }
}
