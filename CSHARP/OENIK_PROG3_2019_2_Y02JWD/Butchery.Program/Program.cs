﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Butchery.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Butchery.Logic;

    /// <summary>
    /// The Program class of the console application.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Displays the main menu items.
        /// </summary>
        /// <returns> String, menu optins. </returns>
        public static string Menu()
        {
            return "Please choose from the following options:\n1. CRUD methods\n2. Non-CRUD methods\n3. Java endpoint\n4. Quit";
        }

        /// <summary>
        /// Displays the CRUD operations.
        /// </summary>
        /// <returns> String, crud options. </returns>
        public static string CRUD()
        {
            return "Please choose from the following options:\n1. Create\n2. Read\n3. Update\n4. Delete\n5. Back";
        }

        /// <summary>
        /// Displays the non-CRUD operations.
        /// </summary>
        /// <returns> String, non-crud operations. </returns>
        public static string Non_CRUD()
        {
            return "Please choose from the following:" +
                "\n1. Average age of customers based on their gender" +
                "\n2. Number of locations based on the used currency" +
                "\n3. Purchases, in which dollar was used as a payment" +
                "\n4. Location with the largest worker count" +
                "\n5. Back";
        }

        /// <summary>
        /// Displays the name of the tables of the database.
        /// </summary>
        /// <returns> String, table names. </returns>
        public static string Tables()
        {
            return "Please choose from the following tables:\n1. Customer\n2. Location\n3. Product\n4. Purchase\n5. Back";
        }

        /// <summary>
        /// Asks the user to decide between readin one or all elements.
        /// </summary>
        /// <returns> String, read one/all. </returns>
        public static string GetOneOrAll()
        {
            return "Please choose from the following options:\n1. Read 1 element\n2. Read all";
        }

        /// <summary>
        /// The main class of the console application.
        /// </summary>
        /// <param name="args"> String array, stores command line arguments. </param>
        public static void Main(string[] args)
        {
            IButcheryLogic logic = new ButcheryLogic();

            string choice;
            while (true)
            {
                Console.WriteLine(Menu());
                choice = Console.ReadLine();
                if (choice == "4")
                {
                    break;
                }

                switch (choice)
                {
                    case "1": // CRUD methods
                        Console.Clear();
                        while (true)
                        {
                            Console.Clear();
                            Console.WriteLine(Tables());
                            string table = Console.ReadLine();
                            if (table == "5")
                            {
                                break;
                            }

                            switch (table)
                            {
                                case "1":
                                    Console.Clear();
                                    Console.WriteLine(CRUD());
                                    string crud1 = Console.ReadLine();
                                    if (crud1 == "5")
                                    {
                                        break;
                                    }

                                    switch (crud1)
                                    {
                                        case "1": // create
                                            Console.Clear();
                                            Console.WriteLine("please enter the following details for customer creation:");
                                            Console.WriteLine("name:");
                                            string name = Console.ReadLine();
                                            Console.WriteLine("age:");
                                            string age_string = Console.ReadLine();
                                            Console.WriteLine("gender:");
                                            string gender = Console.ReadLine();
                                            Console.WriteLine("email address:");
                                            string email = Console.ReadLine();
                                            Console.WriteLine("weight:");
                                            string weight_string = Console.ReadLine();
                                            int age = 0;
                                            int weight = 0;
                                            if (int.TryParse(age_string, out age) && int.TryParse(weight_string, out weight))
                                            {
                                                logic.CreateCustomer(name, age, gender, email, weight);
                                                Console.WriteLine("the new customer was succesfully added");
                                            }
                                            else
                                            {
                                                Console.WriteLine("one or more of the input parameters was of incorrect type");
                                            }

                                            Thread.Sleep(1500);
                                            break;
                                        case "2": // read
                                            Console.Clear();
                                            Console.WriteLine(GetOneOrAll());
                                            string decision = Console.ReadLine();
                                            if (decision == "1")
                                            {
                                                Console.WriteLine("please enter the id of the requested customer:");
                                                string input2 = Console.ReadLine();
                                                if (int.TryParse(input2, out int id))
                                                {
                                                    Console.Clear();
                                                    Console.WriteLine(logic.GetCustomer(id));
                                                }
                                                else
                                                {
                                                    Console.WriteLine("the input wasn't an integer");
                                                }
                                            }
                                            else if (decision == "2")
                                            {
                                                Console.Clear();
                                                Console.WriteLine(logic.GetAllCustomers());
                                            }
                                            else
                                            {
                                                Console.WriteLine("no such option found");
                                            }

                                            Console.WriteLine("\npress any key to continue");
                                            Console.ReadKey();
                                            break;
                                        case "3": // update
                                            Console.Clear();
                                            Console.WriteLine("please enter the id of the customer for email update");
                                            string input3 = Console.ReadLine();
                                            int id3 = 0;
                                            if (int.TryParse(input3, out id3))
                                            {
                                                id3 = int.Parse(input3);
                                                Console.WriteLine("please enter the new email address");
                                                string newEmail = Console.ReadLine();
                                                Console.WriteLine(logic.ChangeEmail(id3, newEmail));
                                            }
                                            else
                                            {
                                                Console.WriteLine("the input wasn't an integer");
                                            }

                                            Thread.Sleep(1500);
                                            break;
                                        case "4": // delete
                                            Console.Clear();
                                            Console.WriteLine("please enter the id of the customer you'd like to delete");
                                            string input4 = Console.ReadLine();
                                            int id4 = 0;
                                            if (int.TryParse(input4, out id4))
                                            {
                                                Console.WriteLine(logic.DeleteCustomer(id4));
                                            }
                                            else
                                            {
                                                Console.WriteLine("the input wasn't an integer");
                                            }

                                            Thread.Sleep(1500);
                                            break;
                                        default:
                                            Console.WriteLine("No such option is found.");
                                            Thread.Sleep(1500); break;
                                    }

                                    break;

                                case "2":
                                    Console.Clear();
                                    Console.WriteLine(CRUD());
                                    string crud2 = Console.ReadLine();
                                    if (crud2 == "5")
                                    {
                                        break;
                                    }

                                    switch (crud2)
                                    {
                                        case "1": // create
                                            Console.Clear();
                                            Console.WriteLine("please enter the following details for location creation");
                                            Console.WriteLine("name:");
                                            string name = Console.ReadLine();
                                            Console.WriteLine("cashier:");
                                            string cashier = Console.ReadLine();
                                            Console.WriteLine("currency:");
                                            string currency = Console.ReadLine();
                                            Console.WriteLine("area:");
                                            string area_string = Console.ReadLine();
                                            Console.WriteLine("worker cout:");
                                            string count_string = Console.ReadLine();
                                            int area = 0;
                                            int count1 = 0;
                                            if (int.TryParse(area_string, out area) && int.TryParse(count_string, out count1))
                                            {
                                                logic.CreateLocation(cashier, name, currency, area, count1);
                                                Console.WriteLine("the new location was succesfully created");
                                            }
                                            else
                                            {
                                                Console.WriteLine("one or more inputs were not in the correct type");
                                            }

                                            Thread.Sleep(1500);
                                            break;
                                        case "2": // read
                                            Console.Clear();
                                            Console.WriteLine(GetOneOrAll());
                                            string decision = Console.ReadLine();
                                            if (decision == "1")
                                            {
                                                Console.WriteLine("please enter the id of the requested location");
                                                string input2 = Console.ReadLine();
                                                if (int.TryParse(input2, out int id))
                                                {
                                                    Console.WriteLine(logic.GetLocation(id));
                                                }
                                                else
                                                {
                                                    Console.WriteLine("the input wasn't an integer");
                                                }
                                            }
                                            else if (decision == "2")
                                            {
                                                Console.WriteLine(logic.GetAllLocations());
                                            }
                                            else
                                            {
                                                Console.WriteLine("no such option found");
                                            }

                                            Console.WriteLine("\npress any key to continue");
                                            Console.ReadKey();
                                            break;
                                        case "3": // update
                                            Console.Clear();
                                            Console.WriteLine("please enter the id of the location for worker count update");
                                            string input3 = Console.ReadLine();
                                            int id3 = 0;
                                            if (int.TryParse(input3, out id3))
                                            {
                                                Console.WriteLine("please enter the new worker count");
                                                string tmp = Console.ReadLine();
                                                if (int.TryParse(tmp, out int newCount))
                                                {
                                                    Console.WriteLine(logic.ChangeWorkerCount(id3, newCount));
                                                }
                                                else
                                                {
                                                    Console.WriteLine("the input wasn't an integer");
                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("the input wasn't an integer");
                                            }

                                            Thread.Sleep(1500);
                                            break;
                                        case "4": // delete
                                            Console.Clear();
                                            Console.WriteLine("please enter the id of the location you'd like to delete");
                                            string input4 = Console.ReadLine();
                                            int id4 = 0;
                                            if (int.TryParse(input4, out id4))
                                            {
                                                Console.WriteLine(logic.DeleteLocation(id4));
                                            }
                                            else
                                            {
                                                Console.WriteLine("the integer wasn't an integer");
                                            }

                                            Thread.Sleep(1500);
                                            break;
                                        default:
                                            Console.WriteLine("No such option is found.");
                                            Thread.Sleep(1500); break;
                                    }

                                    break;

                                case "3":
                                    Console.Clear();
                                    Console.WriteLine(CRUD());
                                    string crud3 = Console.ReadLine();
                                    if (crud3 == "5")
                                    {
                                        break;
                                    }

                                    switch (crud3)
                                    {
                                        case "1": // create
                                            Console.Clear();
                                            Console.WriteLine("please enter the following details for product creation:");
                                            Console.WriteLine("name:");
                                            string name = Console.ReadLine();
                                            Console.WriteLine("price:");
                                            string price_string = Console.ReadLine();
                                            Console.WriteLine("quality:");
                                            string quality = Console.ReadLine();
                                            Console.WriteLine("mass:");
                                            string mass_string = Console.ReadLine();
                                            Console.WriteLine("origin:");
                                            string origin = Console.ReadLine();
                                            int price = 0;
                                            int mass = 0;
                                            if (int.TryParse(price_string, out price) && int.TryParse(mass_string, out mass))
                                            {
                                                logic.CreateProduct(name, price, quality, mass, origin);
                                                Console.WriteLine("new product was succesfully added");
                                            }
                                            else
                                            {
                                                Console.WriteLine("one or more inputs are of incorrect type");
                                            }

                                            Thread.Sleep(1500);
                                            break;
                                        case "2": // read
                                            Console.Clear();
                                            Console.WriteLine(GetOneOrAll());
                                            string decision = Console.ReadLine();
                                            if (decision == "1")
                                            {
                                                Console.WriteLine("please enter the id of the requested product");
                                                string input2 = Console.ReadLine();
                                                if (int.TryParse(input2, out int id))
                                                {
                                                    Console.WriteLine(logic.GetProduct(id));
                                                }
                                                else
                                                {
                                                    Console.WriteLine("the input wasn't an integer");
                                                }
                                            }
                                            else if (decision == "2")
                                            {
                                                Console.WriteLine(logic.GetAllProducts());
                                            }
                                            else
                                            {
                                                Console.WriteLine("no such option found");
                                            }

                                            Console.WriteLine("\npress any key to continue");
                                            Console.ReadKey();
                                            break;
                                        case "3": // update
                                            Console.Clear();
                                            Console.WriteLine("please enter the id of the product for price change");
                                            string input3 = Console.ReadLine();
                                            int id3 = 0;
                                            if (int.TryParse(input3, out id3))
                                            {
                                                Console.WriteLine("please enter the new price");
                                                string tmp = Console.ReadLine();
                                                if (int.TryParse(tmp, out int newPrice))
                                                {
                                                    Console.WriteLine(logic.ChangePrice(id3, newPrice));
                                                }
                                                else
                                                {
                                                    Console.WriteLine("the input wasn't an integer");
                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("the input wasn't an integer");
                                            }

                                            Thread.Sleep(1500);
                                            break;
                                        case "4": // deleteCons
                                            Console.Clear();
                                            Console.WriteLine("please enter the id of the product you'd like to delete");
                                            string input4 = Console.ReadLine();
                                            int id4 = 0;
                                            if (int.TryParse(input4, out id4))
                                            {
                                                Console.WriteLine(logic.DeleteProduct(id4));
                                            }
                                            else
                                            {
                                                Console.WriteLine("the input wasn't an integer");
                                            }

                                            Thread.Sleep(1500);
                                            break;
                                        default:
                                            Console.WriteLine("No such option is found.");
                                            Thread.Sleep(1500); break;
                                    }

                                    break;

                                case "4":
                                    Console.Clear();
                                    Console.WriteLine(CRUD());
                                    string crud4 = Console.ReadLine();
                                    if (crud4 == "5")
                                    {
                                        break;
                                    }

                                    switch (crud4)
                                    {
                                        case "1": // create
                                            Console.Clear();
                                            Console.WriteLine("please enter the details for purchase creation");
                                            Console.WriteLine("customer id:");
                                            string customer_id_string = Console.ReadLine();
                                            Console.WriteLine("product id:");
                                            string product_id_string = Console.ReadLine();
                                            Console.WriteLine("location id:");
                                            string location_id_string = Console.ReadLine();
                                            Console.WriteLine("purchase date in (DD-MM-YYYY) format:");
                                            string date_string = Console.ReadLine();
                                            int customer_id = 0;
                                            int product_id = 0;
                                            int location_id = 0;
                                            DateTime time = DateTime.MinValue;
                                            if (int.TryParse(customer_id_string, out customer_id) &&
                                                int.TryParse(location_id_string, out location_id) &&
                                                int.TryParse(product_id_string, out product_id) &&
                                                DateTime.TryParse(date_string, out time))
                                            {
                                                Console.WriteLine(logic.CreatePurchase(customer_id, product_id, location_id, time));
                                            }
                                            else
                                            {
                                                Console.WriteLine("one or more of the inputs was in incorrect format");
                                            }

                                            Thread.Sleep(1500); break;
                                        case "2": // read
                                            Console.Clear();
                                            Console.WriteLine(GetOneOrAll());
                                            string decision = Console.ReadLine();
                                            if (decision == "1")
                                            {
                                                Console.WriteLine("please enter the id of the requested purchase");
                                                string input2 = Console.ReadLine();
                                                if (int.TryParse(input2, out int id))
                                                {
                                                    Console.WriteLine(logic.GetPurchase(id));
                                                }
                                                else
                                                {
                                                    Console.WriteLine("the input wasn't an integer");
                                                }
                                            }
                                            else if (decision == "2")
                                            {
                                                Console.WriteLine(logic.GetAllPurchases());
                                            }
                                            else
                                            {
                                                Console.WriteLine("no such option found");
                                            }

                                            Console.WriteLine("\npress any key to continue");
                                            Console.ReadKey();
                                            break;
                                        case "3": // update
                                            Console.Clear();
                                            Console.WriteLine("please enter the id for time update");
                                            string input3 = Console.ReadLine();
                                            int id3 = 0;
                                            if (int.TryParse(input3, out id3))
                                            {
                                                Console.WriteLine("please enter the new date (DD-MM-YYYY)");
                                                string tmp = Console.ReadLine();
                                                DateTime newTime = DateTime.MinValue;
                                                if (DateTime.TryParse(tmp, out newTime))
                                                {
                                                    Console.WriteLine(logic.ChangeTime(id3, newTime));
                                                }
                                                else
                                                {
                                                    Console.WriteLine("the input was in an incorrect format");
                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("the input wasn't an integer");
                                            }

                                            Thread.Sleep(1500);
                                            break;
                                        case "4": // delete
                                            Console.Clear();
                                            Console.WriteLine("please enter the id of the purchase you'd like to delete");
                                            string input4 = Console.ReadLine();
                                            int id4 = 0;
                                            if (int.TryParse(input4, out id4))
                                            {
                                                Console.WriteLine(logic.DeletePurchase(id4));
                                            }
                                            else
                                            {
                                                Console.WriteLine("the input wasn't an integer");
                                            }

                                            Thread.Sleep(1500); break;
                                        default:
                                            Console.WriteLine("No such option is found.");
                                            Thread.Sleep(1500); break;
                                    }

                                    break;
                            }
                        }

                        break;

                    case "2": // non-CRUD methods
                        while (true)
                        {
                            Console.Clear();
                            Console.WriteLine(Non_CRUD());
                            string non_crud = Console.ReadLine();
                            if (non_crud == "5")
                            {
                                break;
                            }

                            switch (non_crud)
                            {
                                case "1":
                                    Console.Clear();
                                    foreach (var item in logic.GetCustomerAgeByGender())
                                    {
                                        Console.WriteLine(item);
                                    }

                                    Thread.Sleep(1500);
                                    Console.WriteLine("press any key to continue");
                                    Console.ReadKey();
                                    break;
                                case "2":
                                    Console.Clear();
                                    foreach (var item in logic.GetLocationsPerCurrency())
                                    {
                                        Console.WriteLine(item);
                                    }

                                    Thread.Sleep(1500);
                                    Console.WriteLine("press any key to continue");
                                    Console.ReadKey();
                                    break;
                                case "3":
                                    Console.Clear();
                                    foreach (var item in logic.GetPaidByDollars())
                                    {
                                        Console.WriteLine(item);
                                    }

                                    Thread.Sleep(1500);
                                    Console.WriteLine("press any key to continue");
                                    Console.ReadKey();
                                    break;
                                case "4":
                                    Console.Clear();
                                    foreach (var item in logic.GetLargestLocations())
                                    {
                                        Console.WriteLine(item);
                                    }

                                    Thread.Sleep(1500);
                                    Console.WriteLine("press any key to continue");
                                    Console.ReadKey();
                                    break;
                                default: Console.WriteLine("No such option is found.");
                                    System.Threading.Thread.Sleep(1500); break;
                            }

                            Console.Clear();
                        }

                        break;

                    case "3": // Java endpoint
                        Console.Clear();
                        Console.WriteLine("Please enter the number of new customers to be generated:");
                        string input = Console.ReadLine();
                        int count = 0;
                        if (int.TryParse(input, out count))
                        {
                            Console.WriteLine(logic.JavaGenerator(count));
                        }
                        else
                        {
                            Console.WriteLine("the input wasn't an integer");
                        }

                        Thread.Sleep(1500);
                        Console.WriteLine("press any key to continue");
                        Console.ReadKey();
                        break;
                    default: Console.WriteLine("No such option is found.");
                        System.Threading.Thread.Sleep(1500); break;
                }

                Console.Clear();
            }

            Console.Clear();
            Console.WriteLine("the end\nPlease press any key to exit...");
            Console.ReadKey();
        }
    }
}
