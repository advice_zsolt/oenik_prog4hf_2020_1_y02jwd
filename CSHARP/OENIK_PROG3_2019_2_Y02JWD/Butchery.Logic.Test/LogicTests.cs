﻿// <copyright file="LogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Butchery.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Butchery.Data;
    using Butchery.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class for Unit Testing.
    /// </summary>
    [TestFixture]
    public class LogicTests
    {
        /// <summary>
        /// Initial test.
        /// </summary>
        [Test]
        public void TestTrue()
        {
            Assert.That(true, Is.True);
        }

        /// <summary>
        /// Tests the CreateCustomer method.
        /// </summary>
        [Test]
        public void TestCreateCustomer()
        {
            CUSTOMER testCustomer = new CUSTOMER() { CUSTOMER_ID = 1 };
            Mock<ICustomerRepository> mockedRepo = new Mock<ICustomerRepository>();
            mockedRepo.Setup(x => x.Create(testCustomer));
            ButcheryLogic logic = new ButcheryLogic(mockedRepo.Object);
            logic.CreateCustomer("A", 10, "M", "ASD", 12);
            mockedRepo.Verify(mock => mock.Create(It.IsAny<CUSTOMER>()), Times.Exactly(1));
        }

        /// <summary>
        /// Tests the GetAllCustomers method.
        /// </summary>
        [Test]
        public void TestGetAllCustomers()
        {
            Mock<ICustomerRepository> mockedRepo = new Mock<ICustomerRepository>();
            mockedRepo.Setup(x => x.GetAll());
            ButcheryLogic logic = new ButcheryLogic(mockedRepo.Object);
            logic.GetAllCustomers();
            mockedRepo.Verify(mock => mock.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test the GetCustomer method.
        /// </summary>
        [Test]
        public void TestCustomerGetById()
        {
            Mock<ICustomerRepository> mockedRepo = new Mock<ICustomerRepository>();
            mockedRepo.Setup(x => x.GetById(1));
            ButcheryLogic logic = new ButcheryLogic(mockedRepo.Object);
            logic.GetCustomer(1);
            mockedRepo.Verify(mock => mock.GetById(1), Times.Once);
        }

        /// <summary>
        /// Tests the ChangeEmail method.
        /// </summary>
        [Test]
        public void TestEmailUpdateInCustomer()
        {
            Mock<ICustomerRepository> mockedRepo = new Mock<ICustomerRepository>();
            CUSTOMER test = new CUSTOMER() { CUSTOMER_ID = 1 };
            mockedRepo.Setup(x => x.GetById(1)).Returns(test);
            mockedRepo.Setup(x => x.ChangeEmail(1, "changed"));
            ButcheryLogic logic = new ButcheryLogic(mockedRepo.Object);
            logic.ChangeEmail(1, "changed");
            mockedRepo.Verify(mock => mock.ChangeEmail(1, "changed"), Times.Once);
        }

        /// <summary>
        /// Tests the DeletePurchase method.
        /// </summary>
        [Test]
        public void TestDeleteFromPurchase()
        {
            Mock<IPurchaseRepository> mockedRepo = new Mock<IPurchaseRepository>();
            PURCHASE test = new PURCHASE() { PURCHASE_ID = 1 };
            mockedRepo.Setup(x => x.GetById(1)).Returns(test);
            mockedRepo.Setup(x => x.Delete(test));
            ButcheryLogic logic = new ButcheryLogic(mockedRepo.Object);
            logic.DeletePurchase(1);
            mockedRepo.Verify(mock => mock.Delete(test), Times.Once);
        }

        /// <summary>
        /// Tests the GetCustomerAgeByGender method.
        /// </summary>
        [Test]
        public void TestGetCustomerAgeByGender()
        {
            Mock<ICustomerRepository> mockedRepo = new Mock<ICustomerRepository>();
            List<CUSTOMER> customers = new List<CUSTOMER>()
            {
                new CUSTOMER() { AGE = 20, GENDER = "male" },
                new CUSTOMER() { AGE = 45, GENDER = "female" },
                new CUSTOMER() { AGE = 40, GENDER = "other" },
                new CUSTOMER() { AGE = 60, GENDER = "male" },
                new CUSTOMER() { AGE = 60, GENDER = "other" },
            };
            List<CustomerAgeByGender> expected = new List<CustomerAgeByGender>()
            {
                new CustomerAgeByGender() { Gender = "male", AvgAge = 40 },
                new CustomerAgeByGender() { Gender = "female", AvgAge = 45 },
                new CustomerAgeByGender() { Gender = "other", AvgAge = 50 },
            };
            mockedRepo.Setup(x => x.GetAll()).Returns(customers.AsQueryable());
            ButcheryLogic logic = new ButcheryLogic(mockedRepo.Object);
            var result = logic.GetCustomerAgeByGender();

            Assert.That(result, Is.EquivalentTo(expected));
            Assert.That(result.Count(), Is.EqualTo(3));
            mockedRepo.Verify(mock => mock.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests the GetLocationsPerCurrency method.
        /// </summary>
        [Test]
        public void TestGetLocationsPerCurrency()
        {
            Mock<ILocationRepository> mockedRepo = new Mock<ILocationRepository>();
            List<LOCATION> locations = new List<LOCATION>()
            {
                new LOCATION() { NAME = "USA", CURRENCY = "dollar" },
                new LOCATION() { NAME = "Dagoba", CURRENCY = "ticket" },
                new LOCATION() { NAME = "USA", CURRENCY = "dollar" },
                new LOCATION() { NAME = "Spain", CURRENCY = "euro" },
                new LOCATION() { NAME = "Dagoba", CURRENCY = "ticket" },
            };
            List<LocationsPerCurrency> expected = new List<LocationsPerCurrency>()
            {
                new LocationsPerCurrency() { Currency = "dollar", LocationCount = 2 },
                new LocationsPerCurrency() { Currency = "ticket", LocationCount = 2 },
                new LocationsPerCurrency() { Currency = "euro", LocationCount = 1 },
            };
            mockedRepo.Setup(x => x.GetAll()).Returns(locations.AsQueryable());
            ButcheryLogic logic = new ButcheryLogic(mockedRepo.Object);
            var result = logic.GetLocationsPerCurrency();

            Assert.That(result, Is.EquivalentTo(expected));
            Assert.That(result.Count(), Is.EqualTo(3));
            mockedRepo.Verify(mock => mock.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests the GetPaidByDollars method.
        /// </summary>
        [Test]
        public void TestGetPaidByDollars()
        {
            Mock<IPurchaseRepository> mockedRepo = new Mock<IPurchaseRepository>();
            List<PURCHASE> purchases = new List<PURCHASE>()
            {
                new PURCHASE()
                {
                    CUSTOMER = new CUSTOMER() { NAME = "Bob" },
                    LOCATION = new LOCATION() { NAME = "USA", CURRENCY = "DOLLAR" },
                },
                new PURCHASE()
                {
                    CUSTOMER = new CUSTOMER() { NAME = "Jake" },
                    LOCATION = new LOCATION() { NAME = "Dagoba", CURRENCY = "TICKET" },
                },
                new PURCHASE()
                {
                    CUSTOMER = new CUSTOMER() { NAME = "Mary" },
                    LOCATION = new LOCATION() { NAME = "Spain", CURRENCY = "EURO" },
                },
            };
            List<PaidByDollar> expected = new List<PaidByDollar>()
            {
                new PaidByDollar() { Location = "USA", Name = "Bob" },
            };
            mockedRepo.Setup(x => x.GetAll()).Returns(purchases.AsQueryable);
            ButcheryLogic logic = new ButcheryLogic(mockedRepo.Object);
            var result = logic.GetPaidByDollars();

            Assert.That(result, Is.EquivalentTo(expected));
            Assert.That(result.Count(), Is.EqualTo(1));
            mockedRepo.Verify(mock => mock.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests the GetLargestLocations method.
        /// </summary>
        [Test]
        public void TestGetLargestLocations()
        {
            Mock<ILocationRepository> mockedRepo = new Mock<ILocationRepository>();
            List<LOCATION> location = new List<LOCATION>()
            {
                new LOCATION() { NAME = "Spain", WORKER_COUNT = 5 },
                new LOCATION() { NAME = "Coruscant", WORKER_COUNT = 12 },
                new LOCATION() { NAME = "USA", WORKER_COUNT = 8 },
                new LOCATION() { NAME = "Dagoba", WORKER_COUNT = 7 },
                new LOCATION() { NAME = "Germany", WORKER_COUNT = 3 },
            };
            List<LargestLocation> expected = new List<LargestLocation>()
            {
                new LargestLocation() { Location = "Coruscant", WorkerCount = 12 },
            };
            mockedRepo.Setup(x => x.GetAll()).Returns(location.AsQueryable);
            ButcheryLogic logic = new ButcheryLogic(mockedRepo.Object);
            var result = logic.GetLargestLocations();

            Assert.That(result, Is.EquivalentTo(expected));
            Assert.That(result.Count(), Is.EqualTo(1));
            mockedRepo.Verify(mock => mock.GetAll(), Times.Exactly(2));
        }
    }
}
