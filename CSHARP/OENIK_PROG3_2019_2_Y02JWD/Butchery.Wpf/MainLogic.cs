﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Converters;

namespace Butchery.Wpf
{
    class MainLogic
    {
        string url = "https://localhost:44367/api/CustomerApi/";
        HttpClient client = new HttpClient();
        void SendMessage(bool success)
        {
            string msg = success ? "Operation completed" : "Operation failed";
            Messenger.Default.Send(msg, "CustomerResult");
        }

        public List<CustomerVM> ApiGetCustomers()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<CustomerVM>>(json);
            return list;
        }

        public void ApiDelCustomer(CustomerVM customer)
        {
            bool success = false;
            if (customer != null)
            {
                string json = client.GetStringAsync(url + "del/" + customer.Id.ToString()).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        bool ApiEditCustomer(CustomerVM customer, bool isEditing)
        {
            if (customer == null) return false;
            string myUrl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();

            if (isEditing) postData.Add(nameof(CustomerVM.Id), customer.Id.ToString());
            postData.Add(nameof(CustomerVM.Name), customer.Name);
            postData.Add(nameof(CustomerVM.Age), customer.Age.ToString());
            postData.Add(nameof(CustomerVM.Gender), customer.Gender);
            postData.Add(nameof(CustomerVM.Email), customer.Email);
            postData.Add(nameof(CustomerVM.Weight), customer.Weight.ToString());
            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData))
                .Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }

        public void EditCustomer(CustomerVM customer, Func<CustomerVM,bool> editor)
        {
            CustomerVM clone = new CustomerVM();
            if (customer != null) clone.CopyFrom(customer);
            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (customer != null) success = ApiEditCustomer(clone, true);
                else success = ApiEditCustomer(clone, false);
            }
            SendMessage(success == true);
        }
    }
}
