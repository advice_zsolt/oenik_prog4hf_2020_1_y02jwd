﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Butchery.Wpf
{
    class CustomerVM : ObservableObject
    {
        private int id;
        private string name;
        private int age;
        private string gender;
        private string email;
        private int weight;

        public int Weight
        {
            get { return weight; }
            set { Set(ref weight, value); }
        }

        public string Email
        {
            get { return email; }
            set { Set(ref email, value); }
        }

        public string Gender
        {
            get { return gender; }
            set { Set(ref gender, value); }
        }

        public int Age
        {
            get { return age; }
            set { Set(ref age, value); }
        }

        public string Name
        {
            get { return name; }
            set { Set(ref name,value); }
        }

        public int Id
        {
            get { return id; }
            set { Set(ref id, value); }
        }

        public void CopyFrom(CustomerVM other)
        {
            if (other == null) return;
            this.Id = other.Id;
            this.Name = other.Name;
            this.Age = other.Age;
            this.Gender = other.Gender;
            this.Email = other.Email;
            this.Weight = other.Weight;
        }
    }
}
